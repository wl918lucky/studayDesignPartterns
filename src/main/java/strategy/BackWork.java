package strategy;

/**
 * 策略模式
 * 优点： 1、算法可以自由切换。 2、避免使用多重条件判断。 3、扩展性良好。
 * 缺点： 1、策略类会增多。 2、所有策略类都需要对外暴露。
 * 关键代码：实现同一个接口。
 * 注意事项：如果一个系统的策略多于四个，就需要考虑使用混合模式，解决策略类膨胀的问题。
 */
public class BackWork {

    public void backForWork(Strategy strategy){
        getLicense();
        String s = strategy.strategyOperation();
        System.out.println(s);
        isolationObservation();
    }

    private void getLicense(){
        System.out.println("获取绿色通行码");
        System.out.println("获取社区健康证明");
        System.out.println("获取复工证明");
    }

    private void isolationObservation(){
        System.out.println("隔离观察14天");
    }
}
