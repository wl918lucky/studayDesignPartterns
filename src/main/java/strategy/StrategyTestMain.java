package strategy;

public class StrategyTestMain {
    public static void main(String[] args) {
       BackWork backWork = new BackWork();
//        backWork.backForWork(new PrivateCarStrategy());
//        backWork.backForWork(new GroupCarStrategy());
          backWork.backForWork(new TrainStrategy());
    }
}
