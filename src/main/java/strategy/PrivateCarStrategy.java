package strategy;

public class PrivateCarStrategy implements Strategy{
    @Override
    public String strategyOperation() {
        return "收拾行装用品--->>>自驾出发---->>>到达目的地";
    }
}
