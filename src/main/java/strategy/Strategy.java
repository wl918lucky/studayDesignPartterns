package strategy;

@FunctionalInterface
public interface Strategy {
    public String strategyOperation();
}
