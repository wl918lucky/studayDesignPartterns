package singleton;

public class SingletonTestMain {

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
//                GrilV3 gril = GrilV3.getInstance();
                GrilV4 gril = GrilV4.INSTANCE;
                gril.talking();
                System.out.println(gril.hashCode());
            }).start();
        }
    }
}