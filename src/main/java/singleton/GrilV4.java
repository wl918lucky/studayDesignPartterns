package singleton;

/**
 * 通过枚举，不仅可以解决线程同步，还可以防止反序列化。
 */
public enum  GrilV4 {
    INSTANCE;
    public String talking(){
        return "可以聊天了";
    }
}
