package singleton;

/**
 * 静态内部类方式
 * JVM保证单例
 * 加载外部类时不会加载内部类，这样可以实现懒加载
 */
public class GrilV3 {

    private GrilV3(){}

    public static GrilV3 getInstance(){
        return GrilHolder.INSTANCE;
    }

    private static class GrilHolder{
        private final static GrilV3 INSTANCE = new GrilV3();
    }

}
