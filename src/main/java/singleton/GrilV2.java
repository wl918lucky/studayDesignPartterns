package singleton;


/**
 * lazy loading
 * 也称懒汉式
 * 虽然达到了按需初始化的目的，但却带来线程不安全的问题
 * ①通过方法加 synchronized解决，但也带来效率下降
 * ②通过减小同步代码，双重检查解决
 */
public class GrilV2 {
    //volatile 防止指令重排，而导致出现问题
    private static volatile GrilV2 INSTANCE;

    private GrilV2(){}

    public static GrilV2 getInstance(){
        if(INSTANCE == null){
            synchronized (GrilV2.class) {//减小同步的代码块
                if(INSTANCE == null){//双重检查
                    m1();
                    INSTANCE = new GrilV2();
                }
            }

        }
        return INSTANCE;
    }

    //耗时操作
    private static void  m1(){
        try {
            Thread.sleep(1);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
